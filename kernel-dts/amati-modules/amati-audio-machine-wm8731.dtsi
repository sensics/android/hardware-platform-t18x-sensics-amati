/*
 * amati-audio-wm8731-codec.dtsi - Module defining the audio codec on
 * Amati R1 boards.
 *
 * Copyright (c) 2018 Sensics, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

/ {

	pinmux@2430000 {
		i2s1_active_state: i2s1_active {
			dap1_sclk_pj0 {
				nvidia,pins = "dap1_sclk_pj0";
				nvidia,function = "i2s1";
			};

			dap1_dout_pj1 {
				nvidia,pins = "dap1_dout_pj1";
				nvidia,function = "i2s1";
			};

			dap1_din_pj2 {
				nvidia,pins = "dap1_din_pj2";
				nvidia,function = "i2s1";
			};

			dap1_fs_pj3 {
				nvidia,pins = "dap1_fs_pj3";
				nvidia,function = "i2s1";
			};
		};
	};
	ahub {
		status = "okay";
				/*tegra_i2s1*/
		i2s@2901000 {
			pinctrl-names = "dap_active", "dap_inactive";
			pinctrl-0 = <&i2s1_active_state>;
			pinctrl-1 = <>;
			/*fsync-width = <31>;*/
			status = "okay";
		};
	};
	sound {
		compatible = "sensics,tegra-audio-amati-wm8731";
		nvidia,model = "tegra-snd-t186-amati-wm8731";
		nvidia,num-clk = <8>;
		nvidia,clk-rates = < 270950400	/* PLLA_x11025_RATE */
				     11289600	/* AUD_MCLK_x11025_RATE */
				     45158400	/* PLLA_OUT0_x11025_RATE */
				     45158400	/* AHUB_x11025_RATE */
				     245760000  /* PLLA_x8000_RATE */
				     12288000	/* AUD_MCLK_x8000_RATE */
				     49152000	/* PLLA_OUT0_x8000_RATE */
				     49152000 >;/* AHUB_x8000_RATE */
		clocks = <&tegra_car TEGRA186_CLK_PLLP_OUT0>,
			<&tegra_car TEGRA186_CLK_PLLA>,
			<&tegra_car TEGRA186_CLK_PLL_A_OUT0>,
			<&tegra_car TEGRA186_CLK_AHUB>,
			<&tegra_car TEGRA186_CLK_CLK_M>,
			<&tegra_car TEGRA186_CLK_AUD_MCLK>;
		clock-names = "pll_p_out1", "pll_a", "pll_a_out0", "ahub",
				"clk_m", "extern1";
		resets = <&tegra_car TEGRA186_RESET_AUD_MCLK>;
		reset-names = "extern1_rst";

		nvidia,audio-routing =
			"w LHPOUT",		"w Headphone Jack",
			"w RHPOUT",		"w Headphone Jack",
			"w Mic Bias", "w MICIN",
			"w Mic Jack", "w Mic Bias";

		nvidia,xbar = <&tegra_axbar>;
		nvidia,num-codec-link = <1>;
		status = "okay";
		wm8731_dai_link: nvidia,dai-link-1 {
			link-name = "wm8731-playback";
			cpu-dai = <&tegra_i2s1>;
			codec-dai = <&wm8731_codec>;
			cpu-dai-name = "I2S1";
			codec-dai-name = "wm8731-hifi";
			format = "i2s";
			bitclock-slave;
			frame-slave;
			bitclock-noninversion;
			frame-noninversion;
			bit-format = "s16_le";
			bclk_ratio = <0>;
			srate = <48000>;
			num-channel = <2>;
			name-prefix = "w";
			status = "okay";
		};

	};
};
