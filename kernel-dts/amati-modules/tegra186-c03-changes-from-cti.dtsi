/*
 * Adapted from tegra186-tx2-cti-base.dts Base for CTI Boards
 *
 * Base dtb common to all CTI boards, board specific changes should be made in board specific files that
 * include this file. Originally  based on tegra186-quill-p3310-1000-c03-00-base.dts,
 * merged with tegra186-quill-p3310-1000-a00-00-base.dts, now it just modifies the tree
 * produced by those includes (modified there only to exclude some large includes)
 *
 * Copyright (c) 2015-2017, NVIDIA CORPORATION. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */


/ {

	// Changes to c03 overlaid on a00
	// /delete-node/ xhci@3530000;
	pinctrl@3520000{
		pinmux {
		};
	};
	/*Set the correct lanes and enable the correct PHYs in the board specific files that include this file*/
	pcie-controller@10003000 {
		pci@1,0 {
			// CTICHANGE following line not in original c03 source
			status = "disabled";
		};
		pci@2,0 {
			// CTICHANGE following line not in original c03 source
			status="disabled";
		};
		pci@3,0 {
			// CTICHANGE following line not in original c03 source
			status="disabled";
		};
	};


	i2c@3160000 {
		/*These are not on our boards so delete them to remove the errors*/
		// /delete-node/ lp8557-backlight-s-wuxga-8-0@2c;
		lp8557-backlight-s-wuxga-8-0@2c {
			status = "disabled";
		};
		/delete-node/ ina3221x@42;
		/delete-node/ ina3221x@43;

		// Undo settings change from c03
		ina3221x@40 {
			channel@0 {
				ti,shunt-resistor-mohm = <0x5>;
			};

			channel@1 {
				ti,shunt-resistor-mohm = <0x5>;
			};
		};

		// Undo settings change from c03
		ina3221x@41 {

			channel@0 {
				ti,shunt-resistor-mohm = <0x1>;
			};

			channel@1 {
				ti,shunt-resistor-mohm = <0x5>;
			};

			channel@2 {
				ti,rail-name = "VDD_SYS_SRAM";
				ti,shunt-resistor-mohm = <0x5>;
			};
		};
	};

	//TODO Disable these??
	mttcan@c310000 {
		status = "disabled";
	};

	mttcan@c320000 {
		status = "disabled";
	};

	/*Remove Uneeded PHYS in the board specific files that include this file */
	xhci@3530000 {
		status = "okay";
		phys = <&tegra_xusb_padctl TEGRA_PADCTL_PHY_UTMI_P(0)>,
				<&tegra_xusb_padctl TEGRA_PADCTL_PHY_UTMI_P(1)>,
				<&tegra_xusb_padctl TEGRA_PADCTL_PHY_UTMI_P(2)>,
				<&tegra_xusb_padctl TEGRA_PADCTL_PHY_USB3_P(0)>,
				<&tegra_xusb_padctl TEGRA_PADCTL_PHY_USB3_P(1)>,
				<&tegra_xusb_padctl TEGRA_PADCTL_PHY_USB3_P(2)>;
		phy-names = "utmi-0", "utmi-1", "utmi-2", "usb3-0", "usb3-1", "usb3-2";

		// This property got removed by the removal of the xhci@3530000 section
		// in the original base part derived from the a00 dts.
		// Not sure if we really want it removed, though.
		// nvidia,boost_cpu_freq = <0x320>;
		/delete-property/ nvidia,boost_cpu_freq;
	};

	pinctrl@3520000 {
		/*CTI set this to the always on regulator, we do not have the normal reg on our boards*/
		vbus-2-supply = <&battery_reg>;


		/*Enabled correct phys in the board specific file that includes this file*/
		status = "okay";
		pinctrl-0 = <&tegra_xusb_padctl_pinmux_default>;
		pinctrl-names = "default";

		/delete-property/ pinctrl-1;
		/delete-property/ pinctrl-2;
		/delete-property/ pinctrl-3;
		/delete-property/ pinctrl-4;
		/delete-property/ pinctrl-5;
		/delete-property/ pinctrl-6;
		tegra_xusb_padctl_pinmux_default: pinmux {

			/delete-node/ e3325-usb3-std-A-HS;
			/delete-node/ e3325-usb3-std-A-SS;
			/delete-node/ usb2-micro-AB;
			/delete-node/ usb2-std-A-port2;
			/delete-node/ usb3-std-A-port2;
			usb2-port0 {
				nvidia,lanes = "otg-0";
				nvidia,function = "xusb";
				nvidia,port-cap = <TEGRA_PADCTL_PORT_OTG_CAP>;
				nvidia,oc-pin = <0>;
				status = "okay";
			};
			usb2-port1 {
				nvidia,lanes = "otg-1";
				nvidia,function = "xusb";
				nvidia,port-cap = <TEGRA_PADCTL_PORT_HOST_ONLY>;
				nvidia,oc-pin = <1>;
				status = "okay";
			};
			usb2-port2 {
				nvidia,lanes = "otg-2";
				nvidia,function = "xusb";
				nvidia,port-cap = <TEGRA_PADCTL_PORT_HOST_ONLY>;
				status = "okay";
			};
			usb3-port0 {
				nvidia,lanes = "usb3-0";
				nvidia,port-cap = <TEGRA_PADCTL_PORT_HOST_ONLY>;
				status = "disabled";
			};
			usb3-port1 {
				nvidia,lanes = "usb3-1";
				nvidia,port-cap = <TEGRA_PADCTL_PORT_HOST_ONLY>;
				status = "okay";
			};
			usb3-port2 {
				nvidia,lanes = "usb3-2";
				nvidia,port-cap = <TEGRA_PADCTL_PORT_HOST_ONLY>;
				status = "okay";
			};
		};

	};

	/*We don't use the GPIO expanders for these regulators so remove the gpio properties*/
	 fixed-regulators {
		regulator@13 { //fixes PWM fan
			regulator-always-on;
			/delete-property/ gpio;
		};
	};
};
